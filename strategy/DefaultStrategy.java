package strategy;

import player.State;

public class DefaultStrategy implements Strategy {
    @Override
    public State decide(int points) {
        if(points >= 17) return State.STICK;
        else return State.HIT;
    }
}
