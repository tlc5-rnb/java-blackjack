package strategy;

import player.State;

public class AlwaysStickStrategy implements Strategy{
    @Override
    public State decide(int points) {
        return State.STICK;
    }
}
