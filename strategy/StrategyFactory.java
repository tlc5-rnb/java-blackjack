package strategy;

public class StrategyFactory {
    private StrategyFactory() {}

    public static Strategy strToStrategy(String str){        
        return switch(str){
            case "always-hit" -> new AlwaysHitStrategy();
            case "always-stick" -> new AlwaysStickStrategy();
            case "risk-calculator" -> new RiskCalculatorStratey();
            default -> new DefaultStrategy();
        };
    }
}
