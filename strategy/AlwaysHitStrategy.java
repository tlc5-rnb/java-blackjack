package strategy;

import player.State;

public class AlwaysHitStrategy implements Strategy {
    @Override
    public State decide(int points) {
        return State.HIT;
    }
}
