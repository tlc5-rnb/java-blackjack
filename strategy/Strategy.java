package strategy;

import player.State;

public interface Strategy {
    public State decide(int points);
}
