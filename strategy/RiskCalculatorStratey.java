package strategy;

import player.State;

public class RiskCalculatorStratey implements Strategy {
    @Override
    public State decide(int points) {
        double random = Math.random();
        return random < .5 ? State.HIT : State.STICK;
    }
}
