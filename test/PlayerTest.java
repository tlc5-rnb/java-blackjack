package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import card.Card;
import card.Suit;
import card.Value;
import player.Player;
import blackjack.Dealer;

public class PlayerTest {
    Player p1;
    Card card1;
    Card card2;
    Card card3;
    Dealer dealer;

    @BeforeEach
    public void setup() {
        // p1 = new Player("Player 1");
        dealer = new Dealer();
        card1 = new Card(Suit.SPADE, Value.ACE);
        card2 = new Card(Suit.DIAMOND, Value.THREE);
        card3 = new Card(Suit.CLUB, Value.JACK);
    }

    @Test
    void testTakeCard() {
        // p1.takeCard(card1);
        // assertTrue(card1.isEqual(p1.getHand().get(0)));
    }

    @Test
    void testGetHand() {
        // p1.takeCard(card1);
        // p1.takeCard(card2);
        // p1.takeCard(card3);

        assertTrue(card1.isEqual(p1.getHand().get(0)));
        assertTrue(card2.isEqual(p1.getHand().get(1)));
        assertTrue(card3.isEqual(p1.getHand().get(2)));
    }

}
