package test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

import blackjack.Table;
import player.Player;

public class TableTest {
    


    @Test
    void testGetPlayers() {
        List<String> names = new ArrayList<>();
        names.add("P1");
        names.add("P2");
        names.add("P3");
        Table table1 = new Table();
        List<Player> people = table1.getPlayers();
        assertTrue(people.get(0).getName() == names.get(0));
        assertTrue(people.get(1).getName() == names.get(1));
        assertTrue(people.get(2).getName() == names.get(2));
    }
}
