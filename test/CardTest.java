package test;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import card.Card;
import card.Suit;
import card.Value;

public class CardTest {
    private Card card1;
    private Card card2;
    private Card card3;
    private Card card4;
    private Card card5;

    @BeforeEach
    public void setup() {
        card1 = new Card(Suit.SPADE, Value.ACE);
        card2 = new Card(Suit.DIAMOND, Value.TWO);
        card3 = new Card(Suit.SPADE, Value.ACE);
        card4 = new Card(Suit.HEART, Value.KING);
        card5 = new Card(Suit.CLUB, Value.QUEEN);
    }

    @Test
    void testCompareTo() {
        assertTrue(card1.compareTo(card2) > 0);
        assertTrue(card2.compareTo(card1) < 0);
        assertTrue(card1.compareTo(card3) == 0);
    }

    @Test
    void testEquals() {
        assertTrue(card1.equals(card3));
        assertFalse(card1.equals(card2));
    }

    @Test
    void testGetCardValue() {
        assertEquals(11, card1.getCardValue());
        assertEquals(2, card2.getCardValue());
        assertEquals(11, card3.getCardValue());
        assertEquals(10, card4.getCardValue());
        assertEquals(10, card5.getCardValue());

    }

    @Test
    void testHashCode() {
        assertTrue(card1.hashCode() == card3.hashCode());
        assertFalse(card1.hashCode() == card2.hashCode());
        assertFalse(card2.hashCode() == card3.hashCode());
        assertFalse(card4.hashCode() == card5.hashCode());
    }

    @Test
    void testIsEqual() {
        assertTrue(card1.isEqual(card3));
        assertFalse(card1.isEqual(card2));
        assertFalse(card2.isEqual(card3));
        assertFalse(card4.isEqual(card5));
    }
}
