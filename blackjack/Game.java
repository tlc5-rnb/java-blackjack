package blackjack;

import java.util.ArrayList;
import java.util.List;

import player.Player;
import player.State;
import strategy.StrategyFactory;

public class Game {
    public Table table;
    private Dealer dealer;
    List<String> playerNames = new ArrayList<>();
    private static final int START_CARD_NUMBER = 2;
    
    public Game(String[] args){
        table = new Table();
        dealer = new Dealer();
        setupGame(args);
    }

    private void setupGame(String[] args) {
        int number;
        try{
            if(args.length == 0){
                number= 3;
                setPlayers(number);
            }
            else if (args.length == 1){
                number = Integer.parseInt(args[0]);
                setPlayers(number);
            } else if (args.length == 2){
                number = Integer.parseInt(args[1]);
                setPlayers(number);
            } else {
                setPlayers(args);
            }
        }
        catch (Exception ex){
            number = 3;
        }
        

        
    }

    private void setPlayers(int number){
        if(number < 2 || number > 6) number = 3;
        
        for(int i=0; i<number; ++i){
            String name = "P"+(i+1);
            table.addPlayer(new Player(name, StrategyFactory.strToStrategy("default")));
        }
    }

    private void setPlayers(String[] args) {
        int j = 1;
        for(int i=0; i < args.length; i++) {
            switch(args[i]){
                case "--player":
                    table.addPlayer(new Player("P"+j++, StrategyFactory.strToStrategy(args[i + 1])));
                    break;
                default:
                    continue;
            }
        }
    }

    public void play() {
        int round = 0;
        boolean hasEnded = false;

        while(!hasEnded){
            startRound(round);
            
            System.out.println("\nROUND: "+(round+1));
            System.out.println("=====================");
            table.logPlayers();

            List<Player> bustedPlayers = table.removeBustedPlayers();
            System.out.println(bustedPlayers.size() > 0 ? (bustedPlayers+" were eliminated!") : "No player was eliminated!");

            List<Player> winners = findWinners();
            if(winners.size() >= 1){
                hasEnded = true;
                System.out.println("\n"+winners+" won!");
                System.out.println("Game has ended!");
            }

            ++round;

        }
    }

    private void startRound(int round){
        if(round == 0){
            for (Player player : table.getPlayers()){
                int i = 0;
                while(i < START_CARD_NUMBER){
                    dealer.dealCard(player);
                    ++i;
                }
            }
        }else{
            for (Player player : table.getPlayers()){
                if(player.getState() == State.HIT){
                    dealer.dealCard(player);
                }
            }
        }
    }

    private List<Player> findWinners() {
        List<Player> players = table.getPlayers();
        List<Integer> winnersIndices = new ArrayList<>();
        List<Player> winners = new ArrayList<>();
        int greaterThanCount = 0;
        int stickPlayersCount = 0;

        if(players.size() == 1){
            return players;
        }

        for (int i = 0; i<players.size(); ++i){
            Player currentPlayer = players.get(i);
            int currentPlayerPoints = currentPlayer.calculatePoints();

            if(currentPlayerPoints == 21){
                winnersIndices.add(i);
            }
             else if(currentPlayerPoints >= 17){
                ++greaterThanCount;
            }
             if(currentPlayer.getState() == State.STICK){
                ++stickPlayersCount;
            }
        }

        if(winnersIndices.size() != 0){
            for(var i: winnersIndices){
                winners.add(players.get(i));
            }
        }
        else if(stickPlayersCount == players.size()||greaterThanCount == players.size()){
            int max = 0;
            List<Integer> maxIndices = new ArrayList<>();

            for (int i = 0; i<players.size(); ++i){
                int playerSum = players.get(i).calculatePoints();
                if(playerSum == max){
                    maxIndices.add(i);
                } else if (playerSum > max) {
                    maxIndices.clear();
                    maxIndices.add(i);
                    max = playerSum;
                }
            }

            for (Integer index : maxIndices) {
                winners.add(players.get(index));
            }
        }
        
        return winners;
    }
}