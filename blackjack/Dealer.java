package blackjack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import card.Card;
import card.Suit;
import card.Value;
import player.Player;

public class Dealer {
    private List<Card> deck = new ArrayList<>();

    public Dealer(){
        generateDeck();
        shuffleDeck();
    }

    private void generateDeck(){
        for(Value v: Value.values()){
            for(Suit s: Suit.values()){
                deck.add(new Card(s, v));
            }
        }
    }

    private void shuffleDeck() {
        Collections.shuffle(deck);
    }

    public boolean hasCard() {
        return deck.size() > 0;
    }

    public void dealCard(Player player) {
        int lastIndex = deck.size() - 1;
        Card lastCard = deck.get(lastIndex);

        player.addCard(lastCard);
        deck.remove(lastIndex);
    }

    public List<Card> getDeck(){
        return deck;
    }
    
}
