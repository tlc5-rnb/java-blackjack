 package blackjack;

import java.util.ArrayList;
import java.util.List;

import player.Player;
import player.State;

public class Table {
    private List<Player> players;

    public Table(){
        players = new ArrayList<>();
    }

    public void logPlayers() {
        for(Player player: players){
            System.out.println(player+" ("+player.getState()+")");
        }
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public List<Player> removeBustedPlayers() {
        List<Integer> removeIndices = new ArrayList<>();
        List<Player> newPlayers = new ArrayList<>();
        List<Player> bustedPlayers = new ArrayList<>();

        for(int i=0; i<players.size(); ++i){
            if(players.get(i).getState() == State.BUST){
                removeIndices.add(i);
            }
        }

        for(int i=0; i<players.size(); ++i){
            if(!removeIndices.contains(i)){
                newPlayers.add(players.get(i));
            }else{
                bustedPlayers.add(players.get(i));
            }
        }

        players = newPlayers;
        return bustedPlayers;
    }
}
