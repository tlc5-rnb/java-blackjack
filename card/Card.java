package card;

import java.util.Objects;

public class Card implements Comparable<Card>{
    private final Suit suit;
    private final Value value;

    @Override
    public String toString() {
        return value+""+suit;
    }

    public Card(Suit suit, Value value){
        this.suit = suit;
        this.value = value;
    }
    
    public int getCardValue() {
        return value.getNumber();
    }

    public boolean isEqual(Object obj){
        Card otherCard = (Card)obj;

        return ((otherCard.suit == suit) && 
                otherCard.value == value
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, suit);
    }

    @Override
    public boolean equals(Object obj) {
        Card otherCard = (Card)obj;

        return ((otherCard.suit == suit) && 
                otherCard.value == value
        );
    }

    @Override
    public int compareTo(Card otherCard) {
        int diff = (value.getNumber()) - 
                   (otherCard.value.getNumber());

        return diff == 0 ? (suit.getNumber() - otherCard.suit.getNumber()) : diff;
    }
    
}
