package card;

public enum Suit {
    CLUB(1),
    DIAMOND(2),
    HEART(3),
    SPADE(4);

    private int number;
    private Suit(int n){
        number = n;
    }

    public int getNumber(){
        return number;
    }

    @Override
    public String toString() {
        return ""+this.name().charAt(0);
    }
}
