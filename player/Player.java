package player;

import java.util.ArrayList;
import java.util.List;

import card.Card;
import strategy.DefaultStrategy;
import strategy.Strategy;

public class Player {
    private String name;
    private List<Card> hand;
    private State state;
    private Strategy strategy;

    public Player (String name, Strategy strategy) {
        this.name = name;
        hand = new ArrayList<>();
        this.strategy = strategy;
    }

    public void addCard(Card card) {
        hand.add(card);

        int points = calculatePoints();
        if(points > 21){
            this.state = State.BUST;
        }else{
            this.state = strategy.decide(points);
        }
    }

    public String getName() {
        return name;
    }

    public int calculatePoints() {
        int points = 0;
        
        for (Card card : hand) {
            points += card.getCardValue();
        }

        return points;
    }

	public List<Card> getHand() {
		return hand;
	}

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return name+": "+hand+" -> "+calculatePoints();
    }
}