# Blackjack Game

## Setup
```
cd <project name>
mkdir bin
```

## Running the Application
- Running with default parameters
```
javac -d bin App.java && java -cp ".;bin" App
```

- Running with number of players
```
javac -d bin App.java && java -cp ".;bin" App --seats 4
```

- Set strategies for players
```
javac -d bin App.java && java -cp ".;bin" App --player always-stick --player default --player always-hit --player risk-calculator
```